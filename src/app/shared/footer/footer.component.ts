import { Component, OnInit } from '@angular/core';

declare var jQuery: any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    jQuery(document).ready(function(){
      // Initialize Tooltip
      jQuery('[data-toggle="tooltip"]').tooltip(); 
  });
  }


}
