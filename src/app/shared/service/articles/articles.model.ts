export class Articles {
    author:string;
    title:string;
    description:string;
    url:URL;
    urlToImage:URL;
    publishedAt:Date;
}