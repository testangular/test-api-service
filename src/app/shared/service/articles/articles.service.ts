import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { Articles } from './articles.model';
import { Subject } from 'rxjs/Subject';
import { environment } from './../../../../environments/environment';

@Injectable()
export class ArticlesService {

  private serviceAPI = environment.APIServicesURL + 'v1/articles?source=ars-technica&sortBy=top&apiKey=add7002aec3d4ca4b4327f40a203d67e';
  
  constructor(private http: HttpClient) { }

  getResources(): Observable<Array<Articles>>{
    return this.http.get<Array<Articles>>(this.serviceAPI);
  }

}
