import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleSourceComponent } from './article-source.component';

describe('ArticleSourceComponent', () => {
  let component: ArticleSourceComponent;
  let fixture: ComponentFixture<ArticleSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
