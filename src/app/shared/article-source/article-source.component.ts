import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Articles } from './../service/articles/articles.model';
import { APIService } from './../service/apiServices/api-service.model';
import { ApiServiceService } from './../service/apiServices/api-service.service';

@Component({
  selector: 'app-article-source',
  templateUrl: './article-source.component.html',
  styleUrls: ['./article-source.component.scss']
})
export class ArticleSourceComponent implements OnInit {

  @Input()
  articles: Articles;

  @Input()
  apiService: Array<APIService> = [];

  @Input()
  articleList: Array<Articles> = [];


  constructor( 
    private apiServiceService:ApiServiceService, 
    private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.apiServiceService.getResources().subscribe(apiService=>{
      this.apiService = apiService;
      });
    });  
  }

}
