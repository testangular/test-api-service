import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { FooterComponent } from './footer/footer.component';
import { ArticleSourceComponent } from './article-source/article-source.component';

@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  declarations: [FooterComponent, ArticleSourceComponent],
  exports: [
    FooterComponent
  ]
})
export class SharedModule { }
