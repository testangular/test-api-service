import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticleSourceComponent } from './article-source/article-source.component';

const routes: Routes = [
  { path: 'articles', component: ArticleSourceComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
