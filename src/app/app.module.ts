import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { AppRoutingModule } from './app-routing.module';
import { SharedRoutingModule } from './shared/shared-routing.module';
import { AppComponent } from './app.component';
import { ApiServiceService } from './shared/service/apiServices/api-service.service';
import { ArticlesService } from './shared/service/articles/articles.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    AuthenticationModule,
    SharedRoutingModule,
    HttpClientModule
  ],
  providers: [
    AppRoutingModule,
    ApiServiceService,
    ArticlesService,
    SharedRoutingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
