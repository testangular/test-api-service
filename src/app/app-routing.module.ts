import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedRoutingModule } from './shared/shared-routing.module';

const routes: Routes = [
  { path: '',  pathMatch: 'full', redirectTo: '/articles' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),SharedRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }

